package com.example.tainge.testreview;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Tainge on 11/20/2017.
 */

public class CustomDialog extends DialogFragment {

    Button s1;
    Context context;
    TextView tv1;
    EditText ed1;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.customdialog,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        s1 = view.findViewById(R.id.btn1);
        ed1 = view.findViewById(R.id.ed1);
        tv1 = view.findViewById(R.id.tv1);

        s1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new Msg(ed1.getText().toString()));
              //  dismiss();
            }
        });
    }
}
