package com.example.tainge.testreview;

/**
 * Created by Tainge on 11/20/2017.
 */

public class Msg {
    String msg;

    public Msg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
