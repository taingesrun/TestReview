package com.example.tainge.testreview;

import android.support.v4.view.ViewPager;
import android.view.View;

public class MyTransformer implements ViewPager.PageTransformer {
    @Override
    public void transformPage(View page, float position) {
        int pageWidth = page.getWidth();
        if(position>=1 || position <=-1){
            page.setAlpha(0);
        }else{
            page.setTranslationX(pageWidth * -position);
            page.setAlpha(1 - position);
            page.setScaleX(1-position);
            page.setScaleY(1-position);
        }
    }
}
