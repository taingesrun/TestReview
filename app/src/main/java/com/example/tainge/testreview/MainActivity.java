package com.example.tainge.testreview;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    EditText ed1,ed2;
    int date,month,year;
    Button btn,btncall,btnDate;
    TextView tvShowdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ed1=findViewById(R.id.editext1);
        ed2=findViewById(R.id.editext2);
        btn=findViewById(R.id.btnsend);
        btncall=findViewById(R.id.btncall);



        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,Acitvity2.class);
                String name =ed1.getText().toString();
                String email=ed2.getText().toString();
                intent.putExtra("Name",name);
                intent.putExtra("Email",email);
                startActivity(intent);
            }
        });
        btncall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+ed1.getText().toString()));
//                startActivity(intent);
                CustomDialog cd = new CustomDialog();
                cd.show(getFragmentManager(),"Hello");

            }
        });

        tvShowdate = findViewById(R.id.tvShowDate);
        btnDate  = findViewById(R.id.btndate)     ;
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                date = calendar.get(Calendar.DATE);
                month = calendar.get(Calendar.MONTH);
                year = calendar.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        MainActivity.this,
                        android.R.style.Theme_Holo_Dialog_MinWidth
                        ,new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int date) {
                        tvShowdate.setText(date + "/" + (month+1) + "/" + year);
                    }
                },year,month,date);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                datePickerDialog.show();
            }
        });
    }
    @Subscribe
    public   void getMsg(Msg msg){
        tvShowdate.setText(msg.getMsg());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }
}










