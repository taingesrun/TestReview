package com.example.tainge.testreview;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Spinner;

import com.example.tainge.testreview.adapter.DataAdapter;
import com.example.tainge.testreview.adapter.DataBaseAdapter;

import java.util.Arrays;
import java.util.List;

public class ViewPagerAndFragmentActivity extends AppCompatActivity {

    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager_and_fragment);
        viewPager = findViewById(R.id.pager);
        List<Integer> image = Arrays.asList(R.drawable.tola,R.drawable.mycat,R.drawable.tola,R.drawable.mycat,R.drawable.tola,R.drawable.mycat,R.drawable.tola,R.drawable.tola,R.drawable.tola);
        viewPager.setAdapter(new DataAdapter(this,image));
        viewPager.setPageTransformer(true,new MyTransformer());


        ((Spinner)findViewById(R.id.spinner)).setAdapter(new DataBaseAdapter(this,image));
    }
}
