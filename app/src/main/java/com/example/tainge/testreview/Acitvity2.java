package com.example.tainge.testreview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Acitvity2 extends AppCompatActivity {

    TextView tv1,tv2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acitvity2);
        tv1=findViewById(R.id.text1);
        tv2=findViewById(R.id.text2);

        Intent intent=getIntent();
        String name=intent.getStringExtra("Name");
        String email=intent.getStringExtra("Email");

        tv1.setText(name);
        tv2.setText(email);

    }
}
