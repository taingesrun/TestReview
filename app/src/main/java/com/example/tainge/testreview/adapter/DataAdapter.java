package com.example.tainge.testreview.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.tainge.testreview.R;

import java.util.List;

public class DataAdapter extends PagerAdapter {
    Context cxt;
    List<Integer> image;

    public DataAdapter(Context cxt, List<Integer> image) {
        this.cxt = cxt;
        this.image = image;
    }

    @Override
    public int getCount() {
        return image.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = ((LayoutInflater)cxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.pager_row,container,false);
        ImageView imageView = view.findViewById(R.id.image);
        imageView.setImageResource(image.get(position));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}
