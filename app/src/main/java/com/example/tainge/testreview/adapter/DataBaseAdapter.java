package com.example.tainge.testreview.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.example.tainge.testreview.R;
import java.util.List;
public class DataBaseAdapter extends BaseAdapter {
    private Context cxt;
    private List<Integer> text;
    private ViewHolder viewHolder;

    public DataBaseAdapter(Context cxt, List<Integer> text) {
        this.cxt = cxt;
        this.text = text;
    }

    @Override
    public int getCount() {
        return text.size();
    }

    @Override
    public Integer getItem(int i) {
        return text.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        if(v == null){
            v = ((Activity)cxt).getLayoutInflater().inflate(R.layout.pager_row,viewGroup,false);
            viewHolder= new ViewHolder(v);
            v.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) v.getTag();
        }
        viewHolder.imageView.setImageResource(text.get(i));
        return v;
    }

    class ViewHolder{
        private ImageView imageView;
        public ViewHolder(View view) {
            imageView = view.findViewById(R.id.image);
        }
    }
}
